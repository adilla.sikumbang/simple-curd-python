from flask import Flask, request, jsonify
import psycopg2
import redis

# PostgreSQL configuration
db_config = {
    'dbname': 'simplecurd',
    'user': 'postgres',
    'password': 'ZEWFeEL9gc',
    'host': 'postgres-postgresql',
    'port': '5432',
}

# Redis configuration
redis_config = {
    'host': 'redis-master',
    'port': 6379,
    'db': 0,
}

postgres_conn = psycopg2.connect(**db_config)
redis_conn = redis.StrictRedis(**redis_config)

def create_user(conn, name, email):
    cursor = conn.cursor()
    cursor.execute("INSERT INTO users (name, email) VALUES (%s, %s) RETURNING id", (name, email))
    conn.commit()
    return cursor.fetchone()[0]

def read_user(conn, user_id):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users WHERE id=%s", (user_id,))
    return cursor.fetchone()

def read_all_users(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users")
    return cursor.fetchall()

def update_user(conn, user_id, name, email):
    cursor = conn.cursor()
    cursor.execute("UPDATE users SET name=%s, email=%s WHERE id=%s", (name, email, user_id))
    conn.commit()

def delete_user(conn, user_id):
    cursor = conn.cursor()
    cursor.execute("DELETE FROM users WHERE id=%s", (user_id,))
    conn.commit()

def cache_user(conn, user_id, name, email):
    conn.hset(f"user:{user_id}", {"name": name, "email": email})

def get_cached_user(conn, user_id):
    return conn.hgetall(f"user:{user_id}")

def delete_cached_user(conn, user_id):
    conn.delete(f"user:{user_id}")

app = Flask(__name__)

@app.route('/user', methods=['POST'])
def create_user_endpoint():
    name = request.json['name']
    email = request.json['email']
    user_id = create_user(postgres_conn, name, email)
    cache_user(redis_conn, user_id, name, email)
    return jsonify({'user_id': user_id}), 201

@app.route('/user/<int:user_id>', methods=['GET'])
def read_user_endpoint(user_id):
    user_data = get_cached_user(redis_conn, user_id)
    if not user_data:
        user_data = read_user(postgres_conn, user_id)
        if user_data:
            cache_user(redis_conn, user_id, user_data[1], user_data[2])
            user_data = {"id": user_data[0], "name": user_data[1], "email": user_data[2]}
        else:
            return jsonify({'error': 'User not found'}), 404
    return jsonify(user_data), 200

@app.route('/users', methods=['GET'])
def read_all_users_endpoint():
    users = read_all_users(postgres_conn)
    return jsonify(users), 200

@app.route('/user/<int:user_id>', methods=['PUT'])
def update_user_endpoint(user_id):
    name = request.json['name']
    email = request.json['email']
    update_user(postgres_conn, user_id, name, email)
    cache_user(redis_conn, user_id, name, email)
    return jsonify({'message': f'User with ID {user_id} updated.'}), 200

@app.route('/user/<int:user_id>', methods=['DELETE'])
def delete_user_endpoint(user_id):
    delete_user(postgres_conn, user_id)
    delete_cached_user(redis_conn, user_id)
    return jsonify({'message': f'User with ID {user_id} deleted.'}), 200

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
